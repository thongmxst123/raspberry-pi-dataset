
183-1k5-CarTrafficLight - v2 PredefinedDataset
==============================

This dataset was exported via roboflow.ai on March 29, 2022 at 7:00 AM GMT

It includes 2834 images.
CarTrafficLight are annotated in YOLO v3 Darknet format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 640x360 (Stretch)

No image augmentation techniques were applied.


